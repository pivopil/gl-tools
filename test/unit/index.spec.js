const { groupByTestArray, testArray, testCallbackArray } = require('./dataMocs');
const { uniq, awaitAll, groupBy } = require('../../src/index');

const asyncFn = (v, cb) => setTimeout(() => cb(v), 1000 + Math.random() * 100);

const checkResultCallback = (results) => results.forEach((item, index) => expect(item).toBe(testCallbackArray[index]));

describe('Check uniq function implementation', () => {
  test('uniq function should return an instance of Array', () => expect(Array.isArray(uniq(testArray))).toBe(true));
  test('uniq function should return an array with unique values', () => expect(uniq(testArray)).toMatchSnapshot());
});

describe('Check awaitAll implementation', () => {
  test('checkResultCallback should get testCallbackArray values in correct order',
    () => awaitAll(testCallbackArray.map((it) => asyncFn.bind(null, it)), checkResultCallback));
});

describe('Check groupBy function implementation', () => {
  test('Checked groupBy function using groupByTestArray and groupByTestResult result grouped by key',
    () => expect(groupBy(groupByTestArray, 'key')).toMatchSnapshot());
});
