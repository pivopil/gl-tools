const { groupByReducerTestDataItem } = require('./dataMocs');

const { bindArgsFromN, groupByReducer } = require('../../src/utils');

const sum = (a, b) => a + b;

describe('Check bindArgsFromN implementation', () => {
  test('2 + 3 = 5 using  bindArgsFromN function',
    () => {
      const sumWithFirstArgument = sum.bind(null, 2);
      const result = bindArgsFromN(sumWithFirstArgument, 0, 3)();
      expect(result).toBe(5);
    });
});

describe('Check groupByReducer implementation', () => {
  test('Test groupByReducer with groupByReducerTestDataItem, keyName = testKey',
    () => expect(groupByReducer('testKey')({}, groupByReducerTestDataItem)).toMatchSnapshot());
});
