module.exports = {
  testArray: [1, 1, 2, 2, 3, 4],
  testCallbackArray: [10, 20],
  groupByTestArray: [
    { key: 't_1', v: 'val_1' },
    { key: 't_1', v: 'val_4' },
    { key: 't_2', v: 'val_2' },
    { key: 't_2', v: 'val_3' },
    { key: 't_3', v: 'val_5' },
    { key: 't_3', v: 'val_6' }
  ],
  groupByReducerTestDataItem: { testKey: 'testKey_1', testValue: 'testValue_1' },
};
