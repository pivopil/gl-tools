const path = require('path');

module.exports = {
    rootDir: path.resolve(__dirname, '../'),
    verbose: true,
    testURL: 'http://localhost/',
    moduleFileExtensions: [
        'js',
        'json'
    ],
    moduleNameMapper: {
        '^@/(.*)$': '<rootDir>/src/$1',
    },
    coverageDirectory: '<rootDir>/test/unit/coverage',
    collectCoverageFrom: [
        'src/**/*.{js}',
        '!**/node_modules/**',
    ],
};