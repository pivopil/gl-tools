# Project Title
Data traversal tools

## Description

gl-tools provide APIs for the next operations with arrays:

## Key features
* uniq - Creates a duplicate-value-free version of an array using [ECMAScript 2015 specification value equality](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Set#Value_equality)
* awaitAll - Got list of async functions and callback for the list of results. Results have the same order as the list of async functions. [Partial Application in JavaScript](https://stackoverflow.com/questions/27699493/javascript-partially-applied-function-how-to-bind-only-the-2nd-parameter/30657475)
* groupBy - Creates an object composed of keys generated from the results of running each element of a collection through the callback. [Link with details](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/reduce)

## Examples of usage

### uniq
```
uniq([1, 1, 2, 2, 3, 4])
```
result
```
[1, 2, 3, 4]
```

### awaitAll
```
const asyncFn = (v, cb) => {
    setTimeout(() => {
        cb(v);
    }, 1000 + Math.random() * 100);
}

const resultCallback = (results) => {
    console.log('resultCallback');
    console.log(results[0]); 
    console.log(results[1]); 
}

awaitAll([
    asyncFn.bind(null, 10),
    asyncFn.bind(null, 20)
], resultCallback)
```
result
```
resultCallback
10
20
```

### groupBy
```
const arr = [
    { key: 't_1', v: 'val_1' },
    { key: 't_2', v: 'val_2' },
    { key: 't_2', v: 'val_3' },
    { key: 't_1', v: 'val_4' },
    { key: 't_3', v: 'val_5' },
    { key: 't_3', v: 'val_6' }
];

groupBy(arr, 'key');
```

result
```
{
    't_1': [
        { key: 't_1', v: 'val_1' },
        { key: 't_1', v: 'val_4' }
    ],
    't_2': [
        { key: 't_2', v: 'val_2' },
        { key: 't_2', v: 'val_3' }
    ]
    't_3': [
        { key: 't_3', v: 'val_5' },
        { key: 't_3', v: 'val_6' }
    ]
}
```

## Release 1.0.0
* [Task-1] - defined node modules
* [Task-2] - Add eslint configurations
* [Task-3] - Add gitignore
* [Task-4] - Add Jest configuration
* [Task-5] - Implement helper functions
* [Task-6] - Implement uniq, awaitAll, groupBy functions
* [Task-7] - Add code coverage
* [Task-8] - Update Readme

## Getting Started

```sh
git clone https://pivopil@bitbucket.org/pivopil/gl-tools.git
cd gl-tools
npm i
```

### Prerequisites

You need nodejs and npm to install the software

## Running eslint and tests

```
npm run lint
npm run test
```

## License
MIT