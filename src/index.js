const { wrapToPromise, groupByReducer } = require('./utils');

const uniq = (array) => [...new Set(array)];

const awaitAll = (callbackArray, resultCallback) => Promise.all(callbackArray.map(wrapToPromise)).then(resultCallback);

const groupBy = (array, keyName) => array.reduce(groupByReducer(keyName), {});

module.exports = {
  uniq,
  awaitAll,
  groupBy
};
