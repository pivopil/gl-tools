function bindArgsFromN(fn, n, ...boundArgs) {
  return function (...args) {
    return fn(...args.slice(0, n - 1), ...boundArgs);
  };
}

const wrapToPromise = (func) => new Promise((resolve) => bindArgsFromN(func, 1, resolve)());

const groupByReducer = (keyName) => (accumulator, currentValue) => ({
  ...accumulator,
  [currentValue[keyName]]: accumulator[currentValue[keyName]] ? [...accumulator[currentValue[keyName]], currentValue] : [currentValue]
});

module.exports = {
  bindArgsFromN,
  groupByReducer,
  wrapToPromise
};
